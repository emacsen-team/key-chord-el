(define-package "key-chord" "0.6" "map pairs of simultaneously pressed keys to commands"
  '()
  :url "https://www.emacswiki.org/emacs/download/key-chord.el")
